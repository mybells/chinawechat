import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    	//路由重定向 redirect 重新指定位置
      {
        path: '*',
        redirect: '/',
        meta: {}
      },
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/map',
      name: 'map',
      component: () => import(/* webpackChunkName: "about" */ './views/Map.vue')
    },
    {
      path: '/information',
      name: 'information',
      component: () => import(/* webpackChunkName: "about" */ './views/Information.vue')
    },
    {
      path: '/about/qrcode',
      name: 'qrcode',
      component: () => import(/* webpackChunkName: "about" */ './views/about/Qrcode.vue')
    },
    {
      path: '/map/detail/:id',
      name: 'detail',
      component: () => import(/* webpackChunkName: "about" */ './views/map/Detail.vue')
    },
    {
      path: '/about/feeback',
      name: 'feeback',
      component: () => import(/* webpackChunkName: "about" */ './views/about/Feeback.vue')
    },
    {
      path: '/about/wechat',
      name: 'wechat',
      component: () => import(/* webpackChunkName: "about" */ './views/about/Wechat.vue')
    },
    {
      path: '/about/version',
      name: 'version',
      component: () => import(/* webpackChunkName: "about" */ './views/about/Version.vue')
    },
    {
      path: '/information/content/:id',
      name: 'content',
      component: () => import(/* webpackChunkName: "about" */ './views/information/Content.vue')
    }
  ]
})
